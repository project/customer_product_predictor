CUSTOMER PRODUCT PREDICTOR README

Customer Product Predictor (CPP) is a set of modules designed to run prediction features on your Drupal
site in order to predict the probability of a customer buying your products based on the latest artificial intelligence
algorithms and tools. Development has been driven by two major emphases: flexibility and
usability. So far, we have launch the core module of this product CPP which is fully functional and independent of future
releases related to this product. Also, we have released CPP Ubercart Module as a CPP feature which helps to evaluate the
order comments that your customers write before or after purchasing a order with ubercart system. That's it,
we are building a scalable and flexible system. Furthermore, the administrative interface, particularly relating to the
configuration of your data and its related permissions has been designed for ease of use and efficient display of information.

Customer Product Predictor is being sponsored and developed by the team at
http://www.thinkbinario.com  Sign up for site access to join the thinkBinario community
and take advantage of our forums and issue tracking.

Features overview:
http://www.thinkbinario.com/customer-product-predictor/what_is_cpp

User's and Developer's Guides:
http://www.thinkbinario.com/customer-product-predictor/docs

Installation Manual (with module descriptions):
http://www.thinkbinario.com/customer-product-predictor/installing_cpp

Support forums:
http://www.thinkbinario.com/customer-product-predictor/forum

Contributions directory:
http://www.thinkbinario.com/customer-product-predictor/contrib

Live sites directory:
http://www.thinkbinario.com/customer-product-predictor/site

Report a bug or feature request:
http://drupal.org/project/issues/customer_product_predictor

Thanks for checking out Customer Product Predictor!  We would love to hear your feedback.
We particularly want to know how we can make Customer Product Predictor better and easier to use.
Feel free to post your thoughts in the forums at thinkbinario.com!  We will make a
survey available for more targeted feedback in the near future.

Releases may be downloaded at http://drupal.org/project/customer_product_predictor.

Jose Ortiz,
The ThinkBinario Team

